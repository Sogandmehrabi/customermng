package utility;

import dto.RealCustomer;

import java.util.ArrayList;
import java.util.List;

public class CustomerRepository {
    private static List<RealCustomer> realCustomers=new ArrayList<RealCustomer>();

    public static List<RealCustomer> getRealCustomers() {
        return realCustomers;
    }
    public static void addRealCustomer(RealCustomer customer){
        realCustomers.add(customer);
    }

}
