package utility;

import java.nio.charset.Charset;
import java.util.HashSet;

/**
 * Created by MciCo on 1/18/17.
 */
public class TextHelper {

    private static HashSet<Character> kafSet = new HashSet<>();
    private static HashSet<Character> wawSet = new HashSet<>();
    private static HashSet<Character> hehSet = new HashSet<>();
    private static HashSet<Character> yehSet = new HashSet<>();

    static {
        char[] kaf = {'ک', 'ݢ', 'ݣ', 'ݤ', 'ﮎ', 'ﮏ', 'ﮐ', 'ﮑ', 'ك', 'ګ', 'ڬ', 'ڮ', 'ﻙ', 'ﻚ', 'ﻛ', 'ﻜ'};
        char[] waw = {'ؤ', 'و', 'ٶ', 'ۄ', 'ۊ', 'ۏ', 'ﺅ', 'ﺆ', 'ﻭ', 'ﻮ'};
        char[] heh = {'ه', 'ھ', 'ۀ', 'ہ', 'ۂ', 'ﮤ', 'ﮥ', 'ﮦ', 'ﮪ', 'ﮫ', 'ﮬ', 'ﮭ', 'ﻩ', 'ﻪ', 'ﻫ', 'ﻬ', 'ة', 'ۃ', 'ﺓ', 'ﺔ'};
        char[] yeh = {'ئ', 'ي', 'ۍ', 'ێ', 'ۑ', 'ے', 'ۓ', 'ﮮ', 'ﮯ', 'ﮰ', 'ﮱ', 'ﺉ', 'ﺊ', 'ﺋ', 'ﺌ', 'ﻱ', 'ﻲ', 'ﻳ', 'ﻴ', 'ی', 'ﯼ', 'ﯽ', 'ﯾ', 'ﯿ', 'ى', 'ﻯ', 'ﻰ'};

        for (char aKaf : kaf) kafSet.add(aKaf);
        for (char aWaw : waw) wawSet.add(aWaw);
        for (char aHeh : heh) hehSet.add(aHeh);
        for (char aYeh : yeh) yehSet.add(aYeh);
    }

    public static String toStandardPersian(String s) {
        if (s == null) {
            return null;
        }
        char c;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if (Character.isDigit(c)) sb.append(Character.getNumericValue(c));
            else if (kafSet.contains(c)) sb.append('ک');
            else if (wawSet.contains(c)) sb.append('و');
            else if (hehSet.contains(c)) sb.append('ه');
            else if (yehSet.contains(c)) sb.append('ی');
            else sb.append(c);
        }

        return sb.toString();
    }

    public static String changeDigitsToEnglish(String str) {
        StringBuilder builder = new StringBuilder();
        if (str == null || str.trim().length() == 0) {
            return str;
        }
        for (int i = 0; i < str.length(); i++) {
            char chr = str.charAt(i);
            if (Character.isDigit(chr)) {
                builder.append(Character.getNumericValue(chr));
            } else {
                builder.append(chr);
            }
        }
        return builder.toString();
    }

    public static String encodeUTF8(String text, String source_encoding) {
        if (text == null) {
            return null;
        }
        String ret = text;
        try {
            if (source_encoding == null) {
                source_encoding = "ISO-8859-5";
            }
            Charset UTF8_CHARSET = Charset.forName("UTF-8");
            Charset src_CHARSET = Charset.forName(source_encoding);
            byte[] s1 = text.getBytes(src_CHARSET);
            ret = new String(s1, UTF8_CHARSET);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

}
