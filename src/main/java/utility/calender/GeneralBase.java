package utility.calender;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GeneralBase {

    public static LocalDate getFromString(String str) throws Exception {


        String[] dateStr = str.split("-");

        Integer  year = Integer.valueOf(dateStr[0]);
       Integer month = Integer.valueOf(dateStr[1]);
        if (month < 1 || month > 12) {
            throw new Exception("Month not valid!");
        }

        Integer day = Integer.valueOf(dateStr[2]);
        if (day < 0 || day > 31) {
            throw new Exception("Day not valid!");
        }



        CalendarTool calender = new CalendarTool();
        calender.setIranianDate(year, month, day, 0, 0, 0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return LocalDateTime.parse(calender.getGregorianDate2WithTime(), formatter).toLocalDate();
    }

    public static String convertLocalDateToShamsi(LocalDate localDate) {

       CalendarTool calendarTool = new CalendarTool(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
        Integer irYear = calendarTool.getIranianYear();
       Integer irMonth = calendarTool.getIranianMonth();
        Integer irDay = calendarTool.getIranianDay();

        calendarTool.setIranianDate(irYear, irMonth, irDay, 0, 0, 0);
        calendarTool.getGregorianDate2();

        return calendarTool.getIranianDateTime2().split("\\s")[0];
    }
}
