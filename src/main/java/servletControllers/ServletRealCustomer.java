package servletControllers;

import dto.RealCustomer;
import service.RealCustomerService;
import utility.CustomerRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/ServletRealCustomer")
public class ServletRealCustomer extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String fathername = request.getParameter("fathername");
        String birthDay = request.getParameter("birthDay");
        String nationalCode = request.getParameter("nationalCode");

        RealCustomerService realCustomerService = new RealCustomerService();

        RealCustomer realCustomer = new RealCustomer();
        realCustomer.setName(firstname);
        realCustomer.setCode(nationalCode);
        realCustomer.setBirth(birthDay);
        realCustomer.setFamily(lastname);
        realCustomer.setFatherName(fathername);

        CustomerRepository.addRealCustomer(realCustomer);
        List<RealCustomer> realCustomers=CustomerRepository.getRealCustomers();
        request.setAttribute("RealCustomer", realCustomers);
        RequestDispatcher dispatcher = request.getRequestDispatcher("insert_real_customer.html");
        dispatcher.forward(request, response);


    }

}


