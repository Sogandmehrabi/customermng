package servletControllers;

import dto.LegalCustomer;
import dto.RealCustomer;
import enums.CustomerType;
import exception.CodeException;
import service.LegalCustomerService;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/LegalCustomerInsert")
public class LegalCustomerInsertServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private LegalCustomerService legalCustomerService;

    public void init() {
        this.legalCustomerService = new LegalCustomerService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String code = request.getParameter("code");
        String birth = request.getParameter("birth");


        LegalCustomer legalCustomer = new LegalCustomer(name, code, birth);

        legalCustomer.setCustomerType(CustomerType.LEGAL);

        try {
            try {
                legalCustomerService.insertLegalCustomer(legalCustomer);
            } catch (CodeException e) {
                String customerNumber = legalCustomerService.getCustomerNumber(legalCustomer.getCode());
                request.setAttribute("customerNumber", customerNumber);

                RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
                dispatcher.forward(request, response);
                e.getMessage();

            }
            String customerNumber = legalCustomerService.getCustomerNumber(legalCustomer.getCode());
            request.setAttribute("customerNumber", customerNumber);

            RequestDispatcher dispatcher = request.getRequestDispatcher("success_legal.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}