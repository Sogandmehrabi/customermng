package servletControllers;

import dto.RealCustomer;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/real-customer-list-update-routing")
public class RealCustomerUpdateRouting extends HttpServlet {

    private RealCustomerService realCustomerService;

    @Override
    public void init() throws ServletException {
        realCustomerService = new RealCustomerService();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));

        try {
            RealCustomer realCustomer = realCustomerService.getRealCustomer(customerNumber);
            req.setAttribute("realCustomer", realCustomer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("edit_real_customer.jsp");
        dispatcher.forward(req, resp);
    }
}
