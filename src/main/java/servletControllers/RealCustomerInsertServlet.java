package servletControllers;

import dto.RealCustomer;
import enums.CustomerType;
import exception.CodeException;
import service.RealCustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/RealCustomerInsert")
public class RealCustomerInsertServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private RealCustomerService realCustomerService;

    public void init() {
        this.realCustomerService = new RealCustomerService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String family = request.getParameter("lastName");
        String fatherName = request.getParameter("fatherName");
        String code = request.getParameter("code");
        String birth = request.getParameter("birth");


        RealCustomer realCustomer = new RealCustomer(name, code, birth, family, fatherName);

        realCustomer.setCustomerType(CustomerType.REAL);

        try {
try {
    realCustomerService.insertRealCustomer(realCustomer);
}
            catch (CodeException e) {
                String customerNumber = realCustomerService.getCustomerNumber(realCustomer.getCode());
                request.setAttribute("customerNumber", customerNumber);

                RequestDispatcher dispatcher = request.getRequestDispatcher("error_real.jsp");
                dispatcher.forward(request, response);
                e.getMessage();

            }
            String customerNumber = realCustomerService.getCustomerNumber(realCustomer.getCode());
            request.setAttribute("customerNumber", customerNumber);
            RequestDispatcher dispatcher = request.getRequestDispatcher("success.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}