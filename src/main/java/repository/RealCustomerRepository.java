package repository;

import dto.RealCustomer;
import enums.CustomerType;
import utility.TextHelper;
import utility.calender.GeneralBase;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class RealCustomerRepository {

    public int insertRealCustomer(RealCustomer realCustomer) throws Exception {
        String INSERT_USERS_SQL = "INSERT INTO customer" +
                "  (f_c_name, family, father_name, n_c_code, birth,is_real) VALUES " +
                " (?, ?, ?, ?, ?,?);";


        int result = 0;

        Class.forName("com.mysql.jdbc.Driver");


        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");

            String englishBirth = TextHelper.changeDigitsToEnglish(realCustomer.getBirth());
            LocalDate localDate = GeneralBase.getFromString(englishBirth);

            String englishCode = TextHelper.changeDigitsToEnglish(realCustomer.getCode());
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL);
            preparedStatement.setString(1, realCustomer.getName());
            preparedStatement.setString(2, realCustomer.getFamily());
            preparedStatement.setString(3, realCustomer.getFatherName());
            preparedStatement.setString(4, englishCode);
            preparedStatement.setString(5, localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            preparedStatement.setBoolean(6, CustomerType.REAL.equals(realCustomer.getCustomerType()) ? true : false);


            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            result = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            // process sql exception
            printSQLException(e);
        }
        return result;
    }

    public String getCustomerNumber(String code) throws Exception {

        Class.forName("com.mysql.jdbc.Driver");

        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String SELECT_SQL = "select * from customer where n_c_code = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(SELECT_SQL);

            selectPreparedStatement1.setString(1, code);

            ResultSet resultSet = selectPreparedStatement1.executeQuery();

            while (resultSet.next()) {

                return resultSet.getString("customer_number");

            }
        } catch (SQLException e) {
            // process sql exception
            printSQLException(e);
        }

        return "";


    }

    public List<RealCustomer> getList(Long customerNumber, String firstNamePattern, String lastNamePattern, String nationalCodePattern, String birthdayPattern1, String birthdayPattern2) {

        try {
            Class.forName("com.mysql.jdbc.Driver");


            String selectQuery = "select * from customer c  where\n" +
                    "( ? is null or c.f_c_name like ? ) and\n" +
                    "( ? is null or c.family like ? ) and\n" +
                    "( ? is null or c.n_c_code = ? )  and\n" +
                    "( ? is null or c.customer_number = ?) and\n" +
                    "( ? is null or c.birth>=? )and\n " +
                    "(? is null  or  c.birth<=?) and\n" +
                    "( c.is_real = 1)\n";


            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String SELECT_SQL = "select * from customer where n_c_code = ?";
            PreparedStatement selectPreparedStatement = connection.prepareStatement(selectQuery);

            selectPreparedStatement.setString(1, pattern(firstNamePattern));
            selectPreparedStatement.setString(2, pattern(firstNamePattern));
            selectPreparedStatement.setString(3, pattern(lastNamePattern));
            selectPreparedStatement.setString(4, pattern(lastNamePattern));
            selectPreparedStatement.setString(5, pattern(nationalCodePattern));
            selectPreparedStatement.setString(6, pattern(nationalCodePattern));
            if (customerNumber == null) {
                selectPreparedStatement.setString(7, null);
                selectPreparedStatement.setString(8, null);
            } else {
                selectPreparedStatement.setString(7, String.valueOf(customerNumber));
                selectPreparedStatement.setString(8, String.valueOf(customerNumber));
            }
            if (birthdayPattern1 != null) {
                String englishBirth = TextHelper.changeDigitsToEnglish(birthdayPattern1);
                LocalDate localDate = GeneralBase.getFromString(englishBirth);
                birthdayPattern1=localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            }
            selectPreparedStatement.setString(9, birthdayPattern1);
            selectPreparedStatement.setString(10, birthdayPattern1);
            if (birthdayPattern2 != null) {
                String englishBirth = TextHelper.changeDigitsToEnglish(birthdayPattern2);
                LocalDate localDate = GeneralBase.getFromString(englishBirth);
                birthdayPattern2=localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            }
            selectPreparedStatement.setString(11, birthdayPattern2);
            selectPreparedStatement.setString(12, birthdayPattern2);



            System.out.println(selectPreparedStatement.toString());

             ResultSet resultSet = selectPreparedStatement.executeQuery();

            List<RealCustomer> realCustomers = new LinkedList<>();

            while (resultSet.next()) {

                RealCustomer realCustomer = new RealCustomer();

                realCustomer.setCustomerNumber(resultSet.getInt("customer_number"));
                realCustomer.setName(resultSet.getString("f_c_name"));
                realCustomer.setCode(resultSet.getString("n_c_code"));
                Date date = resultSet.getDate("birth");

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = dateFormat.format(date);

                LocalDate localDate = LocalDate.parse(strDate);
                realCustomer.setBirth(GeneralBase.convertLocalDateToShamsi(localDate));
                realCustomer.setCustomerType(CustomerType.REAL);
                realCustomer.setFamily(resultSet.getString("family"));
                realCustomer.setFatherName(resultSet.getString("father_name"));

                realCustomers.add(realCustomer);

            }
            connection.close();
            return realCustomers;
        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getStackTrace());
            return Collections.emptyList();
        } finally {

        }
    }

    public RealCustomer getRealCustomer(Long customerNumber) {


        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String SELECT_SQL = "select * from customer where customer_number = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(SELECT_SQL);

            selectPreparedStatement1.setInt(1, customerNumber.intValue());

            ResultSet resultSet = selectPreparedStatement1.executeQuery();

            while (resultSet.next()) {

                RealCustomer realCustomer = new RealCustomer();

                realCustomer.setCustomerNumber(resultSet.getInt("customer_number"));
                realCustomer.setName(resultSet.getString("f_c_name"));
                realCustomer.setCode(resultSet.getString("n_c_code"));
                Date date = resultSet.getDate("birth");

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = dateFormat.format(date);

                LocalDate localDate = LocalDate.parse(strDate);
                realCustomer.setBirth(GeneralBase.convertLocalDateToShamsi(localDate));
                realCustomer.setCustomerType(CustomerType.REAL);
                realCustomer.setFamily(resultSet.getString("family"));
                realCustomer.setFatherName(resultSet.getString("father_name"));

                connection.close();
                return realCustomer;

            }
        } catch (Exception e) {
            // process sql exception

            System.out.println(e.getStackTrace());
        }
        return null;
    }

    public void deleteRealCustomer(Long customer_number) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");


            String REMOVE_SQL = "delete from customer where `customer_number` = ?";
            PreparedStatement selectPreparedStatement1 = connection.prepareStatement(REMOVE_SQL);

            selectPreparedStatement1.setInt(1, customer_number.intValue());

            System.out.println(selectPreparedStatement1.toString());

            int result = selectPreparedStatement1.executeUpdate();
            connection.close();

        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getStackTrace());
        }
    }

    public void updateRealCustomer(RealCustomer realCustomer) {

        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://127.0.0.1:3306/customer_management?useSSL=false&useUnicode=true&characterEncoding=utf8", "root", "root");

            String UPDATE_SQL = "UPDATE customer c  " +
                    " set c.n_c_code = ?, c.family = ?, c.f_c_name = ?, c.birth = ?, c.father_name = ? " +
                    " where c.customer_number = ?;";

            String englishBirth = TextHelper.changeDigitsToEnglish(realCustomer.getBirth());
            LocalDate localDate = GeneralBase.getFromString(englishBirth);

            String englishCode = TextHelper.changeDigitsToEnglish(realCustomer.getCode());
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, englishCode);
            preparedStatement.setString(2, realCustomer.getFamily());
            preparedStatement.setString(3, realCustomer.getName());
            preparedStatement.setString(4, localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            preparedStatement.setString(5, realCustomer.getFatherName());
            preparedStatement.setInt(6, realCustomer.getCustomerNumber());

            System.out.println(preparedStatement.toString());

            int result = preparedStatement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            // process sql exception
            System.out.println(e.getMessage());
        }

    }

    private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private String pattern(String str) {
        if (str == null) {
            return str;
        }

        return "%".concat(str).concat("%");
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}


