package service;

import dto.LegalCustomer;
import dto.RealCustomer;
import exception.CodeException;
import repository.LegalCustomerRepository;
import repository.RealCustomerRepository;

import java.util.List;

public class LegalCustomerService {

        private final LegalCustomerRepository legalCustomerRepository;

        public LegalCustomerService() {

       legalCustomerRepository = new LegalCustomerRepository();
        }

        public int insertLegalCustomer(LegalCustomer legalCustomer) throws Exception {
          String code  =legalCustomerRepository.getCustomerNumber(legalCustomer.getCode());
          if(code!=null){
              throw new CodeException("کد اقتصادی معتبر نمیباشد");
          }

            return legalCustomerRepository.insertLegalCustomer(legalCustomer);
        }

        public String getCustomerNumber(String code) throws Exception {
            return legalCustomerRepository.getCustomerNumber(code);
        }

        public List<LegalCustomer> getList(Long customerNumber, String firstNamePattern, String nationalCodePattern,String birthdayPattern1,String birthdayPattern2) {

            return legalCustomerRepository.getList(customerNumber, firstNamePattern, nationalCodePattern,birthdayPattern1,birthdayPattern2);
        }

        public LegalCustomer getLegalCustomer(Long customerNumber) throws Exception {
            return legalCustomerRepository.getLegalCustomer(customerNumber);
        }

        public void updateLegalCustomer(LegalCustomer legalCustomer) throws Exception {
            String code  =legalCustomerRepository.getCustomerNumber(legalCustomer.getCode());
            if(code!=null){
                throw new CodeException("کد اقتصادی معتبر نمیباشد");
            }
            legalCustomerRepository.updateLegalCustomer(legalCustomer);
        }

        public void deleteLegalCustomer(Long customerNumber) throws Exception {
            legalCustomerRepository. deleteLegalCustomer(customerNumber);
        }
}
