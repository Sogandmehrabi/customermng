package service;

import dto.RealCustomer;
import exception.CodeException;
import repository.RealCustomerRepository;

import java.util.List;

public class RealCustomerService {

    private final RealCustomerRepository realCustomerRepository;

    public RealCustomerService() {

        realCustomerRepository = new RealCustomerRepository();
    }

    public int insertRealCustomer(RealCustomer realCustomer) throws Exception {
        String code  =realCustomerRepository.getCustomerNumber(realCustomer.getCode());
        if(code!=null){
            throw new CodeException("کد اقتصادی معتبر نمیباشد");
        }
        return realCustomerRepository.insertRealCustomer(realCustomer);
    }

    public String getCustomerNumber(String code) throws Exception {
        return realCustomerRepository.getCustomerNumber(code);
    }

    public List<RealCustomer> getList(Long customerNumber, String firstNamePattern, String lastNamePattern, String nationalCodePattern,String birthdayPattern1,String birthdayPattern2) {

        return realCustomerRepository.getList(customerNumber, firstNamePattern, lastNamePattern, nationalCodePattern,birthdayPattern1,birthdayPattern2);

    }

    public RealCustomer getRealCustomer(Long customerNumber) throws Exception {
        return realCustomerRepository.getRealCustomer(customerNumber);
    }

    public void updateRealCustomer(RealCustomer realCustomer) throws Exception {
        String code  =realCustomerRepository.getCustomerNumber(realCustomer.getCode());
        if(code!=null){
            throw new CodeException("کد اقتصادی معتبر نمیباشد");
        }
        realCustomerRepository.updateRealCustomer(realCustomer);
    }

    public void deleteRealCustomer(Long customerNumber) throws Exception {
        realCustomerRepository.deleteRealCustomer(customerNumber);
    }

}
