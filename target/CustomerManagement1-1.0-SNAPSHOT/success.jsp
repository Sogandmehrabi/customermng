<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="fa">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>ًResult page</title>
    <style>
        a:link, a:visited {
            background-color: #f44336;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            height:10px;
        }

        a:hover, a:active {
            background-color: cadetblue;
        }
    </style>
</head>
<body bgcolor="#fffff0">
<center>
<h3>عملیات با موفقیت انجام شد</h3>
<%
    request.setCharacterEncoding("UTF-8");
    String realCustomer = (String) request.getAttribute("customerNumber");


%>
<br>
 شماره مشتری<%=realCustomer%>
<br>
<br>
<div class="center-layout">
    <a href="real_customer.html">بازگشت به صفحه مدیریت مشتریان</a>
</div>
</center>
</body>
</html>